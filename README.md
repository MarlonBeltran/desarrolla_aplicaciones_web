**Desarrollo de aplicaciones web con conexion a bases de datos**
**Beltran Gomez Marlon Cristyan**
**5AVP**

-Practica #1 - 02/09/2022 - Practica de ejemplo
Commit: 7e0249bc8ed9f8d2c17fde821f0aa59b0ee887f2
Archivo: https://gitlab.com/MarlonBeltran/desarrolla_aplicaciones_web/-/blob/main/parcial1/practica_ejemplo.html

-Practica #2 - 09/09/2022 - Practica JavaScript
Commit: 3034f7188d296ba9b88e761a2388e0cc6edf492d
Archivo: https://gitlab.com/MarlonBeltran/desarrolla_aplicaciones_web/-/blob/main/parcial1/PracticaJavaScript.html

-Practica #3 - 15/09/2022 - Practica WebDatos
Commit: 68f2787bc723808a860009fc230f9554ce5423b2
Archivo: https://gitlab.com/MarlonBeltran/desarrolla_aplicaciones_web/-/blob/main/parcial1/PracticaWebDatos.rar

-Practica #4 - 19/09/2022 - Practica con bases de datos - Visita de consulta de datos Commit:1868a506ba9d95a72021a3942393a6b545c11aa4
Archivo: https://gitlab.com/MarlonBeltran/desarrolla_aplicaciones_web/-/blob/main/parcial1/PracticaWebDatos/consultarDatos.php

-Practica #5 - 22/09/2022 - Practica web con base de datos - Visita de consulta de datos Commit:ee5b69d381e2c889af6297827782f3468974a1d0
Archivo: https://gitlab.com/MarlonBeltran/desarrolla_aplicaciones_web/-/blob/main/parcial1/PracticaWebDatos/registrarDatos.html

-Practica #6 - 23/09/2022 - Practica Web con bases de datos - Mostrar registros en consulta de datos
consultarDatos.php     -     COMMIT:9c38d5b1a523607b58b759f2143f0754aa81e527
conexion.php           -     COMMIT:92465e14ddfd1bf95b4b3a96ac4ab8fe3f4c9345
